#!/bin/bash

###########################
#Trabalho desenvolvido por Alexandre Dal Zotto Heydrich e Jonathan Peres 
###########################

PROGRAMAS_DIR="./";
FALHAS=0;

function validarDirCasosTeste() {
	# Verifica se a variavel de ambiente TESTCASES_DIR existe 
	# e se foi setada corretamente

	# Para isso, verifica se a variavel está vazia com o programa [ ou test
	# com parametro -z, que verifica se a string da variavel está vazia.
	if [ -z $TESTCASES_DIR ]; then
	# Caso esteja vazia, escreve na tela uma mensagem
	# e realiza a saída do programa com status de terminação 1.
		echo "Variavel de ambiente TESTCASES_DIR não foi declarada.";
        # O Status de terminação 1 é para problemas relacionados a
        # variavel de ambiente TESTCASES_DIR
		exit 1;
	# Caso a variavel exista, então verifica se ela é um diretório
	# Para isso, realiza a verificação com o programa [ ou test novamente
	# com parametro -d, que retorna sucesso caso variavel seja um diretório
	# Como é necessário verificar se não é um diretório, foi passado o !
	# que verifica se o status de terminação é diferente de 0
	# ou seja, se ocorreu algum erro.
	elif [ ! -d $TESTCASES_DIR ]; then
		# Escreve em tela mensagem caso não seja um diretório
		# e sai do programa com status de terminação 1.
		echo "Valor da variavel de ambiente TESTCASES_DIR não se refere à um diretório."
		exit 1;
	fi
}

function validarDirProgramas() {
	# Verifica se foi passado um parametro com o diretório dos programas
	# da equipe ao chamar este programa e se este se refere à um diretório.
	
	# Esta função recebe como parametro o parametro passado ao chamar o programa
	# Então é verificado se o parametro não é uma string vazia
	# com ! -z $1, caso não tenha sido passado nenhum parametro, será uma string vazia
	# e verifica também se, caso não seja uma string vazia, é um diretório
	if [ ! -z $1 ] && [ ! -d $1 ]; then
		# Caso tenha sido passado um parametro e este não seja um diretório
        # exibe uma mensagem de aviso e termina programa com satus de terminação
        # 2, que se refere a problemas com o parametro passado.
        echo "Argumento passado não se refere à um diretório!";
        exit 2;
	# Caso tenha sido passado parametro, este foi validado como diretório no caso acima
	elif [ ! -z $1 ]; then
		# Armazena este valor na variavel PROGRAMAS_DIR
		PROGRAMAS_DIR=$1;
	fi
    	
}

function validarDiretorios() {
	# Realiza devidas validações nos diretórios dos casos de teste
	# e no parametro com programas da equipe.
	
	# Começa validando a variavel de ambiente
	validarDirCasosTeste;
	# Depois valida o parametro passado com o diretório dos programas da equipe
	# de teste, passado ao chamar este programa e passado também como parametro
	# para a função que o valida
	validarDirProgramas $1;
}

function corrigirProgramas() {
	# inicia a correção dos programas da equipe
	# Para isso, é listado todos os programas salvos no diretório
	# de casos de teste.
	# Caso ocorra alguma falha em alguma parte das correções,
	# finaliza a verificação do problema e anota a falha.

	# Correções:
	# - Verifica se o programa foi enviado pela equipe;
	# - Verifica se o programa compila;
	# - Verifica se o programa passa em todos os casos de teste;

	for PROBLEMA in $( ls $TESTCASES_DIR ); do
		if ! verificarProgramaEnviado $PROBLEMA; then
			anotaFalha 1 $PROBLEMA;
		elif ! verificarSeProgramaCompila $PROBLEMA; then
			anotaFalha 2 $PROBLEMA;
		elif ! verificarCasosDeTestesParaPrograma $PROBLEMA; then
			anotaFalha 3 $PROBLEMA;
		fi
	done
	
	return $FALHAS;
}

function anotaFalha() {
	# Quando ocorrer alguma falha, então é chamado esta função
	# Passando como parametro:
	# - Codigo de erro
	# - Problema que ocorreu a falha

	# Verifica o codigo da falha
	# Caso seja 1:
	if [ $1 = 1 ]; then
		# Exibe mensagem que falha de programa não enviado
		echo "Falha: $2 - Programa não enviado.";
	# Caso seja 2:
	elif [ $1 = 2 ]; then
		# Exibe mensagem de falha na compiação
		echo "Falha: $2 - Falha na compilação.";
	# Caso seja 3:
	elif [ $1 = 3 ]; then
		# Exibe mensagem de falha pois não passou nos testes
		echo "Falha: $2 - Não passou nos testes.";
	fi
	
	# Atualiza status de numero de falhas, adicionando +1
	FALHAS=$((FALHAS+1));
}

function verificarProgramaEnviado () {
	#Concatena a extensao de arquivo C no nome do programa recebido
	local PROBLEMA=$1.c;

	#Listar os arquivos do diretorio de programs enviados
	for PROGRAMA in $(ls $PROGRAMAS_DIR| grep .c); do

	#Verifica se o nome do programa recebido por parametro existe no diretorio de programas enviados
	#Caso exista, retorna o status de terminacao 0
		if [ $PROGRAMA = $PROBLEMA ]; then
			return 0;
		fi;
	done;
   
	#Se o programa nao for encontraca no diretorio retorna status de terminacao 1
	return 1;
}

function verificarSeProgramaCompila() {
	#Concatena a extensao de arquivo C no nome do programa recebido
	local PROBLEMA=$1.c;

	#Acessa o diretório dos programas e executa a compilação do arquivo .C do programa recebido por parametro,
	#atráves da chamada do gcc. Também é feito o redirecionamento da saida de erros de complicação para o 'limbo',
	#visando não mostrar o erros de compilação que ocorreram
	COMPILACAO=$(cd $PROGRAMAS_DIR && gcc $PROBLEMA -o $1 2> /dev/null && chmod u+x $1);

	#Retorna o status de terminação do processo de compilação feito pelo gcc
	return $COMPILACAO;
}

function verificarCasosDeTestesParaPrograma () {
	#Concatena a extensao de arquivo C no nome do programa recebido
	local PROBLEMA=$1;

#	 if [ ${PROGRAMAS_DIR: -1} = / ]; then
#		PROGRAMAS_DIR=${PROGRAMAS_DIR:: -1}
#	 fi
    
	# inicia a verificação do casos de testes para o programa recebido por parametro
	# Para isso, é acessado a pasta que estã contidos os casos de testes,
	# listando todos os arquivos de INPUT (que estejam no padrão _INPUT) para o problema/programa recebido por parametro	
	for INPUT in $(cd $TESTCASES_DIR && ls $PROBLEMA | grep _INPUT); do
    	
    	#Extrai o numero do input a partir do nome do arquivo (00_INPUT -> 00)
		local NUMERO_CASO_TESTE=${INPUT%%\_*};

		#Acessa o diretorio de do casos de testes, le o conteudo do INPUT e armazena em uma varíavel
		local ENTRADA_ESPERADA=$(cd $TESTCASES_DIR && cat $PROBLEMA/$INPUT);

		#Acessa o diretorio dos programas e 
		#executa o programa/problema recebido por parametro e que foi compilado pelo gcc , 
		#guardando o retorno da saida da padrão em uma variavel
        SAIDA_PADRAO_PROGRAMA=$(cd $PROGRAMAS_DIR && ./$PROBLEMA $ENTRADA_ESPERADA);

		#Armazena o status de terminacao da execução do ultimo programa/problema executado
		STATUS_TERMINACAO_PROGRAMA=$?;

		#Acessa o diretorio de do casos de testes, le o conteudo do OUTPUT e armazena em uma varíavel
        local RESULTADO_ESPERADO=$(cd $TESTCASES_DIR && cat $PROBLEMA/$NUMERO_CASO_TESTE\_OUTPUT);

		#Verifica se o resultado esperado do problema/programa (conforme definado no arquivo de OUTPUT) 
		#está diferente do resultado obtido com execução do problema/programa 
		#Caso esteja diferente, retorna um status de terminação 1 (status de terminação que indica problemas); Também é feito o redirecionamento da saida de erros de 
		#visando não mostrar quaisquer erros durante a comparação dos valores
		if [ ! $RESULTADO_ESPERADO = $SAIDA_PADRAO_PROGRAMA ] 2> /dev/null; then
			return 1;
		fi;
       
       	#Verifica se o status de terminacao do programa executado foi diferente de zero, ou seja, 
       	#se foi retornado um status diferente do status de sucesso
       	#caso sim, retorna um status de terminação 1 (status de terminação que indica problemas)
		if [ $STATUS_TERMINACAO_PROGRAMA -ne 0 ] 2> /dev/null; then
			return 1;
		fi;
	done;

	return 0;
}

validarDiretorios $1;
corrigirProgramas;